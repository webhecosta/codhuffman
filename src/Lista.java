public class Lista<TIPO> {
    private Folha primeiro;
    private Folha ultimo;
    private int tamanho;

    // CONSTRUTOR VAZIO PARA INICIAR LISTA
    public Lista() {
        zerarLista();
    }

    // ZERAR LISTA
    public void zerarLista() {

        this.primeiro = null;
        this.ultimo = null;
        this.tamanho = 0;
    }

 
    // ADICIONAR LISTA COM NÚMERO DE SALA
    public void adicionarLista(Folha folha) {


        Folha novaFolha = folha;
        if (primeiro == null && ultimo == null) {
            this.primeiro = novaFolha;
            this.ultimo = novaFolha;

        } else {
            this.ultimo.setProximo(novaFolha);
            this.ultimo = novaFolha;
        }
        this.tamanho++;
    }

 
    // // REMOVER PELO VALOR PROCURADO
    public Folha remover(char valorProcurado) throws Exception {
        Folha anterior = null;
        Folha atual = this.primeiro;
        for (int i = 0; i < this.getTamanho(); i++) {

            if (atual.getValue() == (valorProcurado)) { // COMPARAR SE O VALOR ENCONTRADO É O DESEJADO
                if (this.tamanho == 1) {
                    this.primeiro = null;
                    this.ultimo = null;
                } else if (atual == primeiro) {// VERIFICAR SE É O PRIMEIRO ITEM DA LISTA
                    this.primeiro = atual.getProximo();
                    atual.setProximo(null);
                } else if (atual == ultimo) {
                    this.ultimo = anterior;
                    anterior.setProximo(null);
                } else {
                    anterior.setProximo(atual.getProximo()); // AO REMOVER O ITEM, TEM QUE PEGAR O PRÓXIMO APÓS O ITEM
                                                             // REMOVIDO
                    atual = null;
                }
                this.tamanho--;// DIMINUIR O TAMANHO DA LISTA
                break;
            }
            anterior = atual; // ITEM ANTERIOR DA LISTA
            atual = atual.getProximo(); // PRÓXIMO ITEM DA LISTA
        }

        return atual;

    }

    // EXIBIR O Elemento PELA POSIÇÃO
    public Folha get(int posicao) {

        Folha atual = this.primeiro;
        for (int i = 0; i < posicao; i++) {
            if (atual.getProximo() != null) {
                atual = atual.getProximo();
            }
        }
        return atual;
    }
    
    public void set(int posicao, Folha folhaNo) {
        Folha atual = this.primeiro;
        if (posicao == 0) {
            Folha bkProximo = atual.getProximo();
            this.setPrimeiro(folhaNo);
            atual = this.primeiro;
            atual.setProximo(bkProximo);
        } else {

            for (int i = 0; i < posicao; i++) {
                if (i + 1 == posicao) {
                    Folha bkProximo = atual.getProximo();
                    atual.setProximo(folhaNo);
                    atual = atual.getProximo();
                    atual.setProximo(bkProximo);
                    break;
                }
                if (atual.getProximo() != null) {
                    atual = atual.getProximo();
                }

            }
        }
    }

    // EXIBIR O PRIMEIRO Elemento
    public Folha getPrimeiro() {
        return this.primeiro;
    }

    // SETAR O PRIMEIRO
    public void setPrimeiro(Folha primeiro) {
        this.primeiro = primeiro;
    }

    // PEGAR O ÚLTIMO
    public Folha getUltimo() {
        return this.ultimo;
    }

    // SETAR O ÚLTIMO
    public void setUltimo(Folha ultimo) {
        this.ultimo = ultimo;
    }

    // PEGAR O TAMANHO
    public int getTamanho() throws Exception {
        return this.tamanho;
    }

    // SETAR O TAMANHO
    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    // MÉTODO PARA ORDENAR LISTA DE NÓS
    public void ordenarLista(Lista<Arvore> lista) throws Exception {

        for (int i = 0; i < lista.getTamanho(); i++) {
            for (int j = 0; j < lista.getTamanho(); j++) {
                if (lista.get(i).frequency < lista.get(j).frequency) {
                    // aqui acontece a troca, o maior vai para a direita e o menor para a

                    int aux = lista.get(i).frequency;
                    char auxLetra = lista.get(i).value;
                    Arvore esquerda = lista.get(i).getEsquerda();
                    Arvore direita = lista.get(i).getDireita();

                    lista.get(i).setValue(lista.get(j).value);
                    lista.get(j).setValue(auxLetra);
                    lista.get(i).setFrequency(lista.get(j).frequency);
                    lista.get(j).setFrequency(aux);

                    lista.get(i).setEsquerda(lista.get(j).getEsquerda());
                    lista.get(j).setEsquerda(esquerda);
                    lista.get(i).setDireita(lista.get(j).getDireita());
                    lista.get(j).setDireita(direita);
                    // System.out.println("QTDE : "+aux+" | LETRA "+auxLetra);

                }
            }
        }
    }
}
