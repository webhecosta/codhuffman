/*
 * Classe abstrata da arvore 
 * De forma objetiva uma classe abstrata serve como modelo para uma classe concreta, neste caso para as classes NODE e LEAF
 */
abstract class Arvore implements Comparable<Arvore> {

    //public final int frequency; // Frequencia da arvore
    int frequency; // Frequencia da arvore

    public Arvore(int freq) { 
    	frequency = freq; 
    }
    public int getFrequency() {
        return this.frequency;
    }    
    public void setFrequency(int freq) {
        this.frequency = freq ;
    }    

    // Compara as frequencias - Implementacao da Interface Comparable para a ordenacao na fila

    public int compareTo(Arvore arvore) {
        return frequency - arvore.frequency;
    }
}